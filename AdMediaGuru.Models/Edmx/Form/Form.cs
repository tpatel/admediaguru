﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Form.Form")]
    public partial class Form
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Form()
        {
            FormLayouts = new HashSet<FormLayout>();
        }

        public int id { get; set; }

        [StringLength(1000)]
        public string name { get; set; }

        [StringLength(1000)]
        public string formType { get; set; }

        public int? typeId { get; set; }

        [StringLength(100)]
        public string key { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool? isDeleted { get; set; }

        public string description { get; set; }

        public bool? is_system { get; set; }

        public virtual Type Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormLayout> FormLayouts { get; set; }
    }
}
