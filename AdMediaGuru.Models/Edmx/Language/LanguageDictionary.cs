﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Language.LanguageDictionary")]
    public partial class LanguageDictionary
    {
        public int id { get; set; }

        [StringLength(4000)]
        public string name { get; set; }

        [StringLength(4000)]
        public string description { get; set; }

        public byte? languageId { get; set; }

        public int? dictionaryId { get; set; }

        public int? order { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool? isDeleted { get; set; }

        public int? typeId { get; set; }

        public virtual Dictionary Dictionary { get; set; }

        public virtual Language Language { get; set; }

        public virtual Type Type { get; set; }
    }
}
