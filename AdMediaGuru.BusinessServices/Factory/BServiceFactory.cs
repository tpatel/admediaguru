﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices.Factory
{
    public class BServiceFactory
    {
        #region Variable
        private static BServiceFactory _bServiceFactory = null;
        private UserBService _userBService;
        private static UnAuthorizedBService _unAuthorizedBService;
        private ChannelInfoBService _channelInfoBService;
        private ChannelProgrammeBService _channelProgrammeBService;     
        private SubCategory _subCategory;
        private SubCategoryBService _subCategoryBService;
        private CampaignSettingBService _campaignSettingBService;
        private User _user;
        #endregion

        #region Constructor
        public BServiceFactory()
        { }
        #endregion

        #region Factory
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static BServiceFactory GetBServiceFactory()
        {
            return _bServiceFactory = new BServiceFactory();
        }
        #endregion

        #region UserBService

        /// <summary>
        /// UserBService
        /// </summary>
        public UserBService UserBService
        {
            get
            {
                if (this._user == null)
                {
                    this._userBService = new UserBService();
                }
                return _userBService;
            }
        }
        #endregion

        #region UnAuthorizeBService

        /// <summary>
        /// UnAuthorizedBService
        /// </summary>
        public static UnAuthorizedBService UnAuthorizedBService
        {
             get{
                return _unAuthorizedBService = new UnAuthorizedBService();
            }
        }
        #endregion

        #region SubCategory

        /// <summary>
        /// Sub category BService
        /// </summary>
        public SubCategoryBService SubCategoryBService
        {
            get
            {
                if (this._subCategoryBService == null)
                { this._subCategoryBService = new SubCategoryBService(); }
                return _subCategoryBService;
            }
        }

        /// <summary>
        /// Campaign setting BService
        /// </summary>
        public CampaignSettingBService CampaignSettingBService
        {
            get
            {
                if (this._campaignSettingBService == null)
                    this._campaignSettingBService = new CampaignSettingBService();
                return _campaignSettingBService;
            }
        }
        #endregion

        #region ChannelInfoDService
        public ChannelInfoBService ChannelInfoBService
        {
            get
            {
                if (this._channelInfoBService == null)
                    this._channelInfoBService = new ChannelInfoBService();
                return _channelInfoBService;

            }
        }
        #endregion
        #region ChannelProgrammeDService
        public ChannelProgrammeBService ChannelProgrammeBService
        {
            get
            {
                if (this._channelProgrammeBService == null)
                    this._channelProgrammeBService = new ChannelProgrammeBService();
                return _channelProgrammeBService;
            }
        }
        #endregion
    }
}
