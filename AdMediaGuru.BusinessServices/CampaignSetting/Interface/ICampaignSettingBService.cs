﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public interface ICampaignSettingBService
    {
        bool CheckIfCampaignNameExists(string campaignName);
        CampaignSetting CreateCampaignInfo(CampaignSetting campaignData);
    }
}
