﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdMediaGuru.Web
{
    public class CurrentUser
    {
        /// <summary>
        /// Set session keys.
        /// </summary>
        /// <param name="userEmailId"></param>
        /// <param name="userId"></param>
        public static void SetSessionKeys(int user_id,string user_emailId)
        {
            HttpContext.Current.Session["SESSION_CURRENTUSER_USER_ID"] = user_id;
            HttpContext.Current.Session["SESSION_CURRENTUSER_EMAIL_ID"] = user_emailId;
        }

        /// <summary>
        /// Set channel information.
        /// </summary>
        /// <param name="channelId"></param>
        public static void SetChannelInformation(int channelId)
        {
            HttpContext.Current.Session["SESSION_CHANNEL_ID"]= channelId;
        }

        ///<summary>
        ///Set Campaign Setting information
        /// </summary>
        /// <param name="campaignId"></param>
        public static void SetCampaignSettingInfo(int campaignId)
        {
            HttpContext.Current.Session["SESSION_CAMPAIGN_ID"] = campaignId;
        }
    }
}