﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public interface ISubCategoryDService
    {
        List<SubCategory> GetStateData();
        List<SubCategory> GetProductCategory();
        List<SubCategory> GetCampaignObjective();
        List<SubCategory> GetKindOfAudience();
        List<SubCategory> GetRegion();
        List<SubCategory> GetTypeOfChannel();
        List<SubCategory> GetTypeOfChannelGec();
        List<SubCategory> GetTypeOfChannelNews();
        List<SubCategory> GetTelecastCategory();
        List<SubCategory> GetTelecastSchedule();
    }
}
