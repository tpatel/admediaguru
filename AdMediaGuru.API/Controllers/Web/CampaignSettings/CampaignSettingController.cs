﻿using AdMediaGuru.API.Authenticate;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdMediaGuru.API.Controllers
{
    [RoutePrefix("api/v1")]
    public class CampaignSettingController : UnAuthorizeAdMediaGuru
    {
        #region Campign Settings

        [Route("campaignsetting")]
        [HttpPost]
        public HttpResponseMessage PostCampaignInfo(CampaignSetting campaign)
        {
            if (!string.IsNullOrEmpty(campaign.campaignName) && !string.IsNullOrEmpty(campaign.campaignBrandName))
            {
                CampaignSetting campaigninfo = unAuthorizedBService.CreateCampaignInfo(campaign);
                if (campaigninfo != null)
                    return PassResponse(HttpStatusCode.OK, campaigninfo, "Success", "Campaign Created SuccessFully");
                else
                    return PassResponse(HttpStatusCode.InternalServerError, campaigninfo, "Failure", "Already campaign name exists");
            }
            return PassResponse(HttpStatusCode.InternalServerError, null, "Failure", "No Content Found");
        }
        #endregion
    }
}
