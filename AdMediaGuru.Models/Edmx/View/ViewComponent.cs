﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.ViewComponent")]
    public partial class ViewComponent
    {
        public int id { get; set; }

        public int? viewId { get; set; }

        public int? typeId { get; set; }

        public int? componentId { get; set; }

        public int? order { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public virtual Component Component { get; set; }

        public virtual Type Type { get; set; }

        public virtual View View { get; set; }
    }
}
