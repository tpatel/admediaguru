﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public class CampaignSettingBService : BaseBService,ICampaignSettingBService
    {
        public CampaignSetting CreateCampaignInfo(CampaignSetting campaignData)
        {
            bool createcampaigndata = _dServiceFactory.CampaignSettingDService.CheckIfCampaignNameExists(campaignData.campaignName);
            if (createcampaigndata == false)
                return _dServiceFactory.CampaignSettingDService.CreateCampaignInfo(campaignData);
            else
                return null;
        }
        public bool CheckIfCampaignNameExists(string campaignName)
        {
            return _dServiceFactory.CampaignSettingDService.CheckIfCampaignNameExists(campaignName);
        }
    }
}
