﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdMediaGuru.Web.Models
{
    public class AudienceDataModel
    {
        public AudienceBoundData AllAdults { get; set; }
        public AudienceBoundData Male { get; set; }
        public AudienceBoundData Female { get; set; }
        public AudienceBoundData YoungAdults { get; set; }
        public AudienceBoundData Child { get; set; }
    }

    public class AudienceBoundData {
        public Int32 lowerRange { get; set; }
        public Int32 upperRange { get; set; }
    }


    public class RegionDataModel {
       public string[] selectRegion { get; set; }
    }
    public class TypeOfChannelDataModel {
        public TypeOfChannelDataCategory GEC { get; set; }
        public TypeOfChannelDataCategory NEWS { get; set; }
    }
    public class TypeOfChannelDataCategory {
        public string[] time { get; set; }
        public string days { get; set; }
    }
}