﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Property.PropertyComponent")]
    public partial class PropertyComponent
    {
        public int id { get; set; }

        public int? propertyId { get; set; }

        public int? componentId { get; set; }

        [StringLength(1000)]
        public string propertyMode { get; set; }

        public bool? isRequired { get; set; }

        [StringLength(1000)]
        public string componentName { get; set; }

        [StringLength(2000)]
        public string placeholder { get; set; }

        [StringLength(1000)]
        public string helpUrl { get; set; }

        [StringLength(1000)]
        public string target { get; set; }

        [StringLength(2000)]
        public string getValueUrl { get; set; }

        [StringLength(2000)]
        public string setValueUrl { get; set; }

        [StringLength(100)]
        public string navigationUrl { get; set; }

        public bool? isEnable { get; set; }

        [StringLength(1000)]
        public string icon { get; set; }

        public int? dictionaryId { get; set; }

        public int? validationMessageDictionaryId { get; set; }

        public string style { get; set; }

        [StringLength(2000)]
        public string defaultValue { get; set; }

        public int? order { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool? isDeleted { get; set; }

        public int? typeId { get; set; }

        public bool? is_system { get; set; }

        public virtual Component Component { get; set; }

        public virtual Dictionary Dictionary { get; set; }

        public virtual Dictionary Dictionary1 { get; set; }

        public virtual Property Property { get; set; }

        public virtual Type Type { get; set; }
    }
}
