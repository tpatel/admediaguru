﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Campaign.CampaignSetting")]
    public partial class CampaignSetting
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CampaignSetting()
        {
            Audiences = new HashSet<Audience>();
            Regions = new HashSet<Region>();
            TypeOfChannelDayTimes = new HashSet<TypeOfChannelDayTime>();
        }

        [Key]
        public int campaignId { get; set; }

        public int? userId { get; set; }

        public int? channelId { get; set; }

        public int? productCategoryId { get; set; }

        public int? campaignObjectiveId { get; set; }

        public int? typeId { get; set; }

        public string campaignName { get; set; }

        public string campaignBrandName { get; set; }

        public decimal? campaignBudget { get; set; }

        public int? campaignDuration { get; set; }

        public int? campaignTotalFctSeconds { get; set; }

        public int? campaignTotalSpots { get; set; }

        public DateTime? campaignStartDate { get; set; }

        public DateTime? campaignEndDate { get; set; }

        public DateTime? campaignCreatedDate { get; set; }

        public DateTime? campaignUpdatedDate { get; set; }

        public int? campaignCreatedBy { get; set; }

        public bool? campaignIsDeleted { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Audience> Audiences { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Region> Regions { get; set; }

        public virtual SubCategory SubCategory { get; set; }

        public virtual ChannelInfo ChannelInfo { get; set; }

        public virtual SubCategory SubCategory1 { get; set; }

        public virtual Type Type { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TypeOfChannelDayTime> TypeOfChannelDayTimes { get; set; }
    }
}
