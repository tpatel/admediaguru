﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Component")]
    public partial class Component
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Component()
        {
            PropertyComponents = new HashSet<PropertyComponent>();
            ViewComponents = new HashSet<ViewComponent>();
            ViewToolbars = new HashSet<ViewToolbar>();
            ViewTypeComponents = new HashSet<ViewTypeComponent>();
        }

        public int id { get; set; }

        [StringLength(1000)]
        public string name { get; set; }

        [StringLength(100)]
        public string apiUrl { get; set; }

        public int? order { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool? isDeleted { get; set; }

        [StringLength(1000)]
        public string description { get; set; }

        public int? typeId { get; set; }

        public bool? is_system { get; set; }

        public virtual Type Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PropertyComponent> PropertyComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewComponent> ViewComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewToolbar> ViewToolbars { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewTypeComponent> ViewTypeComponents { get; set; }
    }
}
