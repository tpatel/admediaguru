﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.ViewDisplayType")]
    public partial class ViewDisplayType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ViewDisplayType()
        {
            Views = new HashSet<View>();
        }

        public int id { get; set; }

        public int? viewTypeId { get; set; }

        public string name { get; set; }

        public string viewComponent { get; set; }

        public int? order { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public int? typeId { get; set; }

        public virtual Type Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<View> Views { get; set; }

        public virtual ViewType ViewType { get; set; }
    }
}
