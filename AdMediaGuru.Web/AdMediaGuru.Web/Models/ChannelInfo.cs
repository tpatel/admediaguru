﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdMediaGuru.Web.Models
{
    public class ChannelInfo
    {
        public int channel_id { get; set; }
        [Required(ErrorMessage = "Please enter channel name")]
        public string channelname { get; set; }
        [Required(ErrorMessage = "Please enter contact person name")]
        public string contactperson { get; set; }
        [Required(ErrorMessage = "Please enter address")]
        public string channeladdress { get; set; }
        [Required(ErrorMessage = "Please select country")]
        public int? channelcountryId { get; set; }
        [Required(ErrorMessage = "Please select state")]
        public int? channelstateId { get; set; }
        [Required(ErrorMessage = "Please enter email address")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid EmailId")]
        public string channelemail { get; set; }
        [Required(ErrorMessage = "Please enter user password")]
        public string channelpassword { get; set; }
        [Required(ErrorMessage = "Please enter contact number")]
        [Range(0, Int64.MaxValue, ErrorMessage = "Contact number should not contain characters")]
        [StringLength(20, MinimumLength = 10, ErrorMessage = "Contact number should have minimum 11 digits")]
        public string contactnumber { get; set; }
        public bool is_deleted { get; set; }
    }
}