﻿using AdMediaGuru.BusinessServices.Factory;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace AdMediaGuru.API.Authenticate
{
   // [Authorize]
    public class AuthorizeAdMediaGuru : ApiController
    {
        #region Variable
        private BServiceFactory _bServiceFactory = null;
        #endregion

        #region Other
        /// <summary>
        /// Authorize BService
        /// </summary>
        public BServiceFactory AuthoriseBService
        {
            get
            {
                if (_bServiceFactory == null)
                    _bServiceFactory = BServiceFactory.GetBServiceFactory();
                return _bServiceFactory;
            }
        }

        /// <summary>
        /// Json media type formatter.
        /// </summary>
        protected JsonMediaTypeFormatter jsonMediaTypeFormatter = new JsonMediaTypeFormatter()
        {
            SerializerSettings = {
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore
            }
        };

        /// <summary>
        /// Response message.
        /// </summary>
        /// <param name="resultType"></param>
        /// <param name="resultData"></param>
        /// <param name="messageTitle"></param>
        /// <param name="messageDescription"></param>
        /// <returns></returns>
        public HttpResponseMessage PassResponse(HttpStatusCode resultType,object resultData,string messageTitle,string messageDescription)
        {
            var response = new ResponseMessage
            {
                resultType = resultType,
                resultData = resultData,
                messageTitle = messageTitle,
                messageDescription = messageDescription
            };
            return new HttpResponseMessage(resultType)
            {
                Content = new ObjectContent<ResponseMessage>(response, jsonMediaTypeFormatter)
            };
        }
        #endregion
    }
}