﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdMediaGuru.Web.Models
{
    public class User
    {
        public int user_id { get; set; }
        [Required(ErrorMessage = "Please select state")]
        public string user_state { get; set; }
        [Required(ErrorMessage = "Please enter user name")]
        public string user_name { get; set; }
        [Required(ErrorMessage = "Please enter company name")]
        public string user_company_name { get; set; }
        [Required(ErrorMessage = "Please enter email id")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid EmailId")]
        public string user_emailId { get; set; }
        [Required(ErrorMessage = "Please enter mobile number")]
        public string user_phone { get; set; }
        [Required(ErrorMessage = "Please enter user password")]
        public string user_password { get; set; }
        public bool is_public { get; set; }
        public bool is_deleted { get; set; }
    }
}