﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdMediaGuru.Models;

namespace AdMediaGuru.DataServices
{
    public interface IUserDService
    {
        User CreateUser(User userData);
        bool CheckIfUserRegistar(string emailId);
        User Authenticate(string emailId, string password);
        bool ForgetPassword(string emailId);
    }
}
