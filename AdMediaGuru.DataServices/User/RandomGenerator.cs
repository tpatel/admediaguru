﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public static class RandomGenerator
    {
        /// <summary>
        /// Generate a random number between 2 numbers 
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        /// <summary>
        /// Generate random string with given size.
        /// </summary>
        /// <param name="size"></param>
        /// <param name="lowerCase"></param>
        /// <returns></returns>
        public static string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int randomSize = 0; randomSize < size; randomSize++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        /// <summary>
        /// Generate random password.
        /// </summary>
        /// <returns></returns>
        public static string RandomPassword()
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append(RandomString(4, true));
            strBuilder.Append(RandomNumber(1000, 9999));
            strBuilder.Append(RandomString(2, false));
            return strBuilder.ToString();
        }
    }
}
