﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.ViewCard")]
    public partial class ViewCard
    {
        public int id { get; set; }

        public int? viewId { get; set; }

        public int? typeId { get; set; }

        [StringLength(1000)]
        public string backgroundColor { get; set; }

        [StringLength(1000)]
        public string statusStripColor { get; set; }

        public string getUrl { get; set; }

        public bool? isMenuVisible { get; set; }

        [StringLength(1000)]
        public string sort { get; set; }

        public bool isDesc { get; set; }

        [StringLength(1000)]
        public string displayLabel { get; set; }

        public bool isAddVisible { get; set; }

        public int? order { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public int? createdBy { get; set; }

        public int? updatedBy { get; set; }

        public string itemTypeKey { get; set; }

        public bool isEditable { get; set; }

        public virtual Type Type { get; set; }

        public virtual View View { get; set; }
    }
}
