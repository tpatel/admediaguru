﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Property.Property")]
    public partial class Property
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Property()
        {
            FormLayouts = new HashSet<FormLayout>();
            PropertyComponents = new HashSet<PropertyComponent>();
            ViewLayouts = new HashSet<ViewLayout>();
            ViewLevels = new HashSet<ViewLevel>();
            ViewLevels1 = new HashSet<ViewLevel>();
            ViewProperties = new HashSet<ViewProperty>();
        }

        public int id { get; set; }

        public int? typeId { get; set; }

        [StringLength(1000)]
        public string name { get; set; }

        [StringLength(4000)]
        public string displayName { get; set; }

        public bool? isDefaultDisplay { get; set; }

        [StringLength(4000)]
        public string updateKey { get; set; }

        [StringLength(4000)]
        public string sortKey { get; set; }

        public bool? isDesc { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public DateTime? isDeleted { get; set; }

        public bool isNullAllowed { get; set; }

        [StringLength(100)]
        public string dataType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormLayout> FormLayouts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PropertyComponent> PropertyComponents { get; set; }

        public virtual Type Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLayout> ViewLayouts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLevel> ViewLevels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLevel> ViewLevels1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewProperty> ViewProperties { get; set; }
    }
}
