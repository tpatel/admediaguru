﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Log.Error")]
    public partial class Error
    {
        public int id { get; set; }

        [StringLength(1000)]
        public string moduleName { get; set; }

        [StringLength(1000)]
        public string typeName { get; set; }

        [StringLength(1000)]
        public string methodName { get; set; }

        [StringLength(1000)]
        public string source { get; set; }

        public string message { get; set; }

        public string stackTrace { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public int? typeId { get; set; }

        public virtual Type Type { get; set; }
    }
}
