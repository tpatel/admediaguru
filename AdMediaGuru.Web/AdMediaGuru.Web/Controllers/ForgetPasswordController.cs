﻿using AdMediaGuru.Models;
using AdMediaGuru.Web.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdMediaGuru.Web.Controllers
{
    public class ForgetPasswordController : AuthorizeWebAdMediaGuru
    {
        // GET: ForgetPassword
        public ActionResult ForgetPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgetPassword(User userForgetPassword)
        {
            bool passwordUpdate = AuthorizeBService.UserBService.ForgetPassword(userForgetPassword.user_emailId);
            if (passwordUpdate == true)
                return Json("Password Updated",JsonRequestBehavior.AllowGet);
            else
            return Json("Unable to upadate password",JsonRequestBehavior.AllowGet);
        }
    }
}