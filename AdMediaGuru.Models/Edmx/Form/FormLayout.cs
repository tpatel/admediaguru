﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Form.FormLayout")]
    public partial class FormLayout
    {
        public int id { get; set; }

        public int? formId { get; set; }

        public int? propertyId { get; set; }

        [StringLength(4000)]
        public string inputMode { get; set; }

        [StringLength(4000)]
        public string style { get; set; }

        public int? order { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public int? typeId { get; set; }

        public bool? is_system { get; set; }

        public virtual Form Form { get; set; }

        public virtual Property Property { get; set; }

        public virtual Type Type { get; set; }
    }
}
