﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public interface IChannelInfoDService
    {
        ChannelInfo CreateChannelData(ChannelInfo channelData);
        bool CheckIfChannelExists(string channelName);
        bool CheckIfChannelUserRegistar(string emailId);
    }
}
