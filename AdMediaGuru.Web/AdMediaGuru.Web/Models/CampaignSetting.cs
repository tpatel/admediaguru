﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AdMediaGuru.Web.Models
{
    public class CampaignSetting
    {
        public int campaignId { get; set; }
        public int? userId { get; set; }
        public int? channelId { get; set; }
        public int? productCategoryId { get; set; }
        public int? campaignObjectiveId { get; set; }
        public int? typeId { get; set; }
        [Required(ErrorMessage = "Please enter campaign name.")]
        public string campaignName { get; set; }
        [Required(ErrorMessage = "Please enter brand name.")]
        public string campaignBrandName { get; set; }
        [Required(ErrorMessage = "Please enter budget.")]
        public decimal? campaignBudget { get; set; }
        [Required(ErrorMessage = "Please enter duration .")]
        public int? campaignDuration { get; set; }
        [Required(ErrorMessage = "Please enter total fct seconds.")]
        public int? campaignTotalFctSeconds { get; set; }
        [Required(ErrorMessage = "Please enter total spots.")]
        public int? campaignTotalSpots { get; set; }
        [Required(ErrorMessage = "Please select start date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? campaignStartDate { get; set; }
        [Required(ErrorMessage = "Please select end date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? campaignEndDate { get; set; }
        public DateTime? campaignCreatedDate { get; set; }
        public DateTime? campaignUpdatedDate { get; set; }
        public int? campaignCreatedBy { get; set; }
        public bool? campaignIsDeleted { get; set; }
    }

    
}
