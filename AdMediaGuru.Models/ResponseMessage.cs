﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    public class ResponseMessage
    {
        #region Parameters
        public HttpStatusCode resultType { get; set; }
        public object resultData { get; set; }
        public string messageTitle { get; set; }
        public string messageDescription { get; set; }
        //public long totalCount { get; set; }
        #endregion
    }
}
