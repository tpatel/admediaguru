﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace AdMediaGuru.Models.DTO
{
    public class StandardUtility
    {
        public static string PasswordEncoding(string password)
        {
            byte[] originalBytes;byte[] encodedBytes;
            MD5 md5 = new MD5CryptoServiceProvider();
            originalBytes = Encoding.Default.GetBytes(password);
            encodedBytes = md5.ComputeHash(originalBytes);
            return BitConverter.ToString(encodedBytes);
        }

        public static string GetIPAddress
        {
            get
            { 
            try
            {
                    return System.Net.Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress.ToString()).GetValue(0).ToString();
            }
            catch(Exception ex)
            { return "::1"; }
            }
        }
    }
}
