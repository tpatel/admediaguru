﻿using AdMediaGuru.Models;
using AdMediaGuru.Web.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdMediaGuru.Web.Controllers
{
    public class ChannelProgrammeController : AuthorizeWebAdMediaGuru
    {
        // GET: ChannelProgramme
        public ActionResult ChannelProgramme()
        {
            try
            {
                //CurrentUser.SetChannelInformation(1);
                if (HttpContext.Session["SESSION_CHANNEL_ID"].ToString() != "" && HttpContext.Session["SESSION_CHANNEL_ID"].ToString() != null)
                {
                    ViewBag.channelId = Convert.ToInt32(Session["SESSION_CHANNEL_ID"]);
                    List<SubCategory> getProgrammeType = AuthorizeBService.SubCategoryBService.GetTelecastCategory();
                    ViewBag.getTelecastCategory = getProgrammeType;
                    List<SubCategory> getTelecastSchedule = AuthorizeBService.SubCategoryBService.GetTelecastSchedule();
                    ViewBag.getTelecastSchedule = getTelecastSchedule;
                    return View();
                }
                else
                    return RedirectToAction("ChannelInformation", "ChannelInformation");
            }
            catch (Exception ex)
            { return RedirectToAction("ChannelInformation", "ChannelInformation"); }
            //return View();
        }

        ///<summary>
        ///save channel programme data
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChannelProgramme(ChannelProgramme channelProgrammeData)
        {
            try
            {
                var checkProgrammeData = AuthorizeBService.ChannelProgrammeBService.CheckIfProgrammeExists(channelProgrammeData.channelprogrammename);
                if (checkProgrammeData == true)
                    return Json("ProgrammeExists", JsonRequestBehavior.AllowGet);
                else 
                {
                    var addProgrammeData = AuthorizeBService.ChannelProgrammeBService.CreateChannelProgrammeData(channelProgrammeData);
                    if (addProgrammeData != null)
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    else
                        return Json("Failure", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }
    }
}