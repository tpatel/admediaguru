﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.ViewLevel")]
    public partial class ViewLevel
    {
        public int id { get; set; }

        public int? viewId { get; set; }

        public int? levelOneTypeId { get; set; }

        public int? levelOnePriorityId { get; set; }

        public int? levelTwoTypeId { get; set; }

        public int? levelTwoPriorityId { get; set; }

        public bool? isLevelOneAddVisible { get; set; }

        public bool? isLevelTwoAddVisible { get; set; }

        [StringLength(1000)]
        public string levelOneSort { get; set; }

        [StringLength(1000)]
        public string levelTwoSort { get; set; }

        public bool? isLevelOneMenuAllow { get; set; }

        public bool? isLevelTwoMenuAllow { get; set; }

        public bool? isLevelOneDesc { get; set; }

        public bool? isLevelTwoDesc { get; set; }

        [StringLength(1000)]
        public string levelOneLabel { get; set; }

        [StringLength(1000)]
        public string levelTwoLabel { get; set; }

        [StringLength(1000)]
        public string levelOneDefaultSearchCriteria { get; set; }

        [StringLength(1000)]
        public string levelOneDefaultSearchKey { get; set; }

        [StringLength(1000)]
        public string levelTwoDefaultSearchCriteria { get; set; }

        [StringLength(1000)]
        public string levelTwoDefaultSearchKey { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public bool isLevelOneNullValueAllowed { get; set; }

        public bool isLevelTwoNullValueAllowed { get; set; }

        [StringLength(1000)]
        public string levelOneNullValueTitle { get; set; }

        [StringLength(1000)]
        public string levelTwoNullValueTitle { get; set; }

        public bool isLevelOneEditable { get; set; }

        public bool isLevelTwoEditable { get; set; }

        public virtual Property Property { get; set; }

        public virtual Property Property1 { get; set; }

        public virtual Type Type { get; set; }

        public virtual Type Type1 { get; set; }

        public virtual View View { get; set; }
    }
}
