﻿using AdMediaGuru.API.Authenticate;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AdMediaGuru.API.Controllers
{
    [RoutePrefix("api/v1")]
    public class UserController : AuthorizeAdMediaGuru
    {
        #region User API

        /// <summary>
        /// Create user function.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("user")]
        public HttpResponseMessage PostUser(User user)
        {
            if (!string.IsNullOrEmpty(user.user_emailId) && !string.IsNullOrEmpty(user.user_phone))
            {
                User addUserResult = AuthoriseBService.UserBService.CreateUser(user);
                if (addUserResult != null)
                    return PassResponse(HttpStatusCode.OK, addUserResult, "Success", "User created successfully");
                else
                    return PassResponse(HttpStatusCode.InternalServerError, addUserResult, "Failure", "User already exists.");
            }
            return PassResponse(HttpStatusCode.InternalServerError, null, "Failure", "Model data are empty.");
        }

        /// <summary>
        /// Check user credentials.
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        [Route("login")]
        [HttpPost]
        public HttpResponseMessage LoginUser(User user)
        {
            User usersDetails = AuthoriseBService.UserBService.Authenticate(user.user_emailId, user.user_password);
            if (usersDetails != null)
                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new ObjectContent<User>(usersDetails, jsonMediaTypeFormatter)
                };
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound)
                {
                    Content = new ObjectContent<object>("message: Incorrect emailId or Password", jsonMediaTypeFormatter)
                };
        }

        /// <summary>
        /// Forget password.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("forgetpassword")]
        public HttpResponseMessage ForgetUserPassword(User userForgetPassword)
        {
            bool passwordUpdate = AuthoriseBService.UserBService.ForgetPassword(userForgetPassword.user_emailId);
            if (passwordUpdate)
                return PassResponse(HttpStatusCode.OK, null, "Success", "Password updated.");
            else
                return PassResponse(HttpStatusCode.OK, null, "Error", "Unable to update password.");
        }
    }
    #endregion
}

