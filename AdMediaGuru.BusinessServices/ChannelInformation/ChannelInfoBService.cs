﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public class ChannelInfoBService : BaseBService, IChannelInfoBService
    {
        /// <summary>
        /// CreateChannelData
        /// </summary>
        /// <param name="channelData"></param>
        /// <returns></returns>
        public ChannelInfo CreateChannelData(ChannelInfo channelData)
        {   
                return _dServiceFactory.ChannelInfoDService.CreateChannelData(channelData);
        }

        /// <summary>
        /// CheckIfChannelExists
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        public bool CheckIfChannelExists(string channelName)
        {
            return _dServiceFactory.ChannelInfoDService.CheckIfChannelExists(channelName);
        }

        /// <summary>
        /// Check if channel user registar.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public bool CheckIfChannelUserRegistar(string emailId)
        {
            return _dServiceFactory.ChannelInfoDService.CheckIfChannelUserRegistar(emailId);
        }
    }
}
