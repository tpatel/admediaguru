﻿using AdMediaGuru.DataServices.GenericRepository;
using AdMediaGuru.Models;
using AdMediaGuru.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public class ChannelInfoDService : IChannelInfoDService
    {
        #region Context
        protected GenericRepository<ChannelInfo> EDMXInstanceChannelInfo { get; set; }
        protected GenericRepository<User> EDMXInstanceUser { get; set; }
        public readonly Expression<Func<ChannelInfo, ChannelInfoDTO>> selectUserExpression;

        #endregion

        #region Constructor
        public ChannelInfoDService()
        {
            this.EDMXInstanceChannelInfo = new GenericRepository<ChannelInfo>();
            this.EDMXInstanceUser = new GenericRepository<User>();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Create ChannelInfo
        /// </summary>
        /// <param name="channelData"></param>
        /// <returns></returns>
        public ChannelInfo CreateChannelData(ChannelInfo channelData)
        {
            try
            {
                User addUser = new User()
                {
                    user_name = channelData.contactperson,
                    user_company_name = channelData.channelname,
                    user_emailId = channelData.channelemail,
                    user_password = StandardUtility.PasswordEncoding(channelData.channelpassword),
                    is_public = false,
                    is_deleted = false,
                    created_date = DateTime.UtcNow
                };
                this.EDMXInstanceUser.DbSet.Add(addUser);
                this.EDMXInstanceUser.Context.SaveChanges();
                int userId = addUser.user_id;

                ChannelInfo objChannelData = new ChannelInfo()
                {
                    channelname = channelData.channelname,
                    user_id = userId,
                    contactperson = channelData.contactperson,
                    channeladdress = channelData.channeladdress,
                    contactnumber = channelData.contactnumber,
                    channelcountryId = channelData.channelcountryId,
                    channelstateId = channelData.channelstateId,
                    channelemail = channelData.channelemail,
                    is_deleted = false,
                    createddate = DateTime.UtcNow
                };
                this.EDMXInstanceChannelInfo.DbSet.Add(objChannelData);
                this.EDMXInstanceChannelInfo.Context.SaveChanges();

                return ConvertChannelInfoModel(objChannelData);
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Convert channel info model.
        /// </summary>
        /// <param name="newChannelInfo"></param>
        /// <returns></returns>
        private ChannelInfo ConvertChannelInfoModel(ChannelInfo newChannelInfo)
        {
            return new ChannelInfo()
            {
                channel_id = newChannelInfo.channel_id,
                channelemail = newChannelInfo.channelemail,
                user_id = newChannelInfo.user_id
            };
        }

        /// <summary>
        /// Check if channel name exists or not.
        /// </summary>
        /// <param name="channelName"></param>
        /// <returns></returns>
        public bool CheckIfChannelExists(string channelName)
        {
            return (this.EDMXInstanceChannelInfo.DbSet.Count(channeldata => channeldata.channelname == channelName && channeldata.is_deleted == false) > 0 ? true : false);
        }

        /// <summary>
        /// Check if channel user registar.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public bool CheckIfChannelUserRegistar(string emailId)
        {
            return (this.EDMXInstanceUser.DbSet.Count(user => user.user_emailId == emailId && user.is_deleted == false && user.is_public == false) > 0 ? true : false);
        }

        #endregion

    }
}
