﻿using AdMediaGuru.DataServices;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{   
    public class DServiceFactory
    {
        #region Variables
        private static DServiceFactory _dServiceFactory = null;
        private UserDService _userDService;
        private CampaignSettingDService _campaignSettingDService;
        private SubCategoryDService _subCategoryDService;
        private ChannelInfoDService _channelInfoDService;
        private ChannelProgrammeDService _channnelProgrammeDService;
        private User _user;
        private SubCategory _subCategory;
        private ChannelInfo _channelInfo; 
               
        #endregion

        #region Constructor
        public DServiceFactory() { }
        #endregion

        #region FactoryMethods
        /// <summary>
        /// GetDServiceFactory
        /// </summary>
        /// <returns></returns>
        public static DServiceFactory GetDServiceFactory()
        {
            return _dServiceFactory = new DServiceFactory();
        }
        #endregion

        #region UserDService

        /// <summary>
        /// UserDService
        /// </summary>
        public UserDService UserDService
        {
            get
            {
                if (this._user == null)
                {
                    this._userDService = new UserDService();
                }
                return _userDService;
            }
        }
        #endregion

        #region CampaignSettingDService
        /// <summary>
        /// CampaignSettingDService
        /// </summary>
        public CampaignSettingDService CampaignSettingDService
        {
            get
            {
                if (this._campaignSettingDService == null)
                {
                    this._campaignSettingDService = new CampaignSettingDService();
                }
                return _campaignSettingDService;
            }
        }
        #endregion

        #region SubCategoryDService
        public SubCategoryDService SubCategoryDService
        {
            get
            {
                if (this._subCategoryDService == null)
                    this._subCategoryDService = new SubCategoryDService();
                return _subCategoryDService;
            }
        }
        #endregion

        #region ChannelInfoDService
        public ChannelInfoDService ChannelInfoDService
        {
            get
            {
                if (this._channelInfoDService == null)
                    this._channelInfoDService = new ChannelInfoDService();
                return _channelInfoDService;
                        
            }
        }
        #endregion

        #region ChannelProgrammeDService
        public ChannelProgrammeDService ChannelProgrammeDService
        {
            get
            {
                if (this._channnelProgrammeDService == null)
                    this._channnelProgrammeDService = new ChannelProgrammeDService();
                return _channnelProgrammeDService;
            }
        }
        #endregion
    }



}
