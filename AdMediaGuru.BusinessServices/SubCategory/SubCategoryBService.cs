﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public class SubCategoryBService : BaseBService,ISubCategoryBService
    {
        public List<SubCategory> GetStateData() { return _dServiceFactory.SubCategoryDService.GetStateData(); }
        public List<SubCategory> GetProductCategory() { return _dServiceFactory.SubCategoryDService.GetProductCategory(); }
        public List<SubCategory> GetCampaignObjective() { return _dServiceFactory.SubCategoryDService.GetCampaignObjective(); }
        public List<SubCategory> GetKindOfAudience() { return _dServiceFactory.SubCategoryDService.GetKindOfAudience(); }
        public List<SubCategory> GetRegion() { return _dServiceFactory.SubCategoryDService.GetRegion(); }
        public List<SubCategory> GetTypeOfChannel() { return _dServiceFactory.SubCategoryDService.GetTypeOfChannel(); }
        public List<SubCategory> GetTypeOfChannelGec() { return _dServiceFactory.SubCategoryDService.GetTypeOfChannelGec(); }
        public List<SubCategory> GetTypeOfChannelNews() { return _dServiceFactory.SubCategoryDService.GetTypeOfChannelNews(); }
        public List<SubCategory> GetCountryData() { return _dServiceFactory.SubCategoryDService.GetCountryData(); }
        public List<SubCategory> GetTelecastCategory() { return _dServiceFactory.SubCategoryDService.GetTelecastCategory(); }
        public List<SubCategory> GetTelecastSchedule() { return _dServiceFactory.SubCategoryDService.GetTelecastSchedule(); }
    }
}
