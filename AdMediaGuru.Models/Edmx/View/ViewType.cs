﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.ViewType")]
    public partial class ViewType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ViewType()
        {
            Views = new HashSet<View>();
            ViewDisplayTypes = new HashSet<ViewDisplayType>();
            ViewTypeComponents = new HashSet<ViewTypeComponent>();
        }

        public int id { get; set; }

        [StringLength(4000)]
        public string name { get; set; }

        public int? order { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public int? typeId { get; set; }

        public virtual Type Type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<View> Views { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewDisplayType> ViewDisplayTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewTypeComponent> ViewTypeComponents { get; set; }
    }
}
