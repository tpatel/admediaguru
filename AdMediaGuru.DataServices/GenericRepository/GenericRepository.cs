﻿using AdMediaGuru.DataServices.Edmx;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices.GenericRepository
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal admedia_guruEntities Context;
        internal DbSet<TEntity> DbSet;
        internal CultureInfo enGB;
        public GenericRepository()
        {
            this.Context = new admedia_guruEntities();
            this.DbSet = Context.Set<TEntity>();
            this.enGB = new CultureInfo("en-GB");
        }
    }
}
