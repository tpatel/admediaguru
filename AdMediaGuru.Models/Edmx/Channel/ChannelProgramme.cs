﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Channel.ChannelProgramme")]
    public partial class ChannelProgramme
    {
        public int channelprogrammeid { get; set; }
        public int? channelId { get; set; }
        public int? channelprogrammetypecategoryId { get; set; }
        public int? channeltelecastscheduleId { get; set; }
        public TimeSpan? channeltelecasttime { get; set; }
        public TimeSpan? channelprimetimefrom { get; set; }
        public TimeSpan? channelprimetimeto { get; set; }
        public TimeSpan? channelnonprimetimefrom { get; set; }
        public TimeSpan? channelnonprimetimeto { get; set; }
        public string channelurl { get; set; }
        [StringLength(4000)]
        public string channelprogrammename { get; set; }
        public virtual SubCategory SubCategory { get; set; }
        public virtual SubCategory SubCategory1 { get; set; }
        public virtual ChannelInfo ChannelInfo { get; set; }
    }
}
