﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdMediaGuru.Models
{
    [Table("Category.Category")]
    public partial class Category
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Category()
        {
            SubCategories = new HashSet<SubCategory>();
        }

        [Key]
        public int category_id { get; set; }

        [StringLength(1000)]
        public string category_name { get; set; }

        [StringLength(4000)]
        public string category_description { get; set; }

        [StringLength(500)]
        public string category_key { get; set; }

        public DateTime? created_date { get; set; }

        public bool? is_deleted { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubCategory> SubCategories { get; set; }
    }
}
