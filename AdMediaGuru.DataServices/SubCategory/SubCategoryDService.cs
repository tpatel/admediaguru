﻿using AdMediaGuru.DataServices.GenericRepository;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public class SubCategoryDService : ISubCategoryDService
    {
        #region Context
        protected GenericRepository<SubCategory> EDMX { get; set; }
        #endregion

        #region Constructor

        //Constructor.
        public SubCategoryDService()
        {
            this.EDMX = new GenericRepository<SubCategory>();
        }
        #endregion

        #region SubCategory Method

        /// <summary>
        /// Get State Data from subcategories.
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetStateData()
        {
            try
            {
                List<SubCategory> getStateData = this.EDMX.Context.SubCategories.Where(c => c.category_id == 1).ToList();
                //SubCategoryCollection subCategoryCollection = new SubCategoryCollection();
                //subCategoryCollection.AddRange(this.EDMX.Context.SubCategories.Where(c => c.category_id == 1)
                //    .Select(c => new SubCategoryDTO()
                //    {
                //        sub_category_id = c.sub_category_id,
                //        category_text = c.category_text
                //    }).ToList());
                return getStateData;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// GetProductCategory
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetProductCategory()
        {
            try
            {
                List<SubCategory> getProductCategory = this.EDMX.Context.SubCategories.Where(c => c.category_id == 4).ToList();
                return getProductCategory;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// GetCampaignObjective
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetCampaignObjective()
        {
            try
            {
                List<SubCategory> getCampaignObjective = this.EDMX.Context.SubCategories.Where(c => c.category_id == 6).ToList();
                return getCampaignObjective;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// GetKindOfAudience
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetKindOfAudience()
        {
            try
            {
                List<SubCategory> getKindOfAudience = this.EDMX.Context.SubCategories.Where(c => c.category_id == 5).ToList();
                return getKindOfAudience;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Get region data.
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetRegion()
        {
            try
            {
                List<SubCategory> getRegion = this.EDMX.Context.SubCategories.Where(c => c.category_id == 8).ToList();
                return getRegion;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        /// <summary>
        /// Get type of channel data.
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetTypeOfChannel()
        {
            try
            {
                List<SubCategory> getTypeOfChannel = this.EDMX.Context.SubCategories.Where(c => c.category_id == 7 && c.is_parentId ==null).ToList();
                return getTypeOfChannel;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        /// <summary>
        /// Get type of channel gec.
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetTypeOfChannelGec()
        {
            try
            {
                List<SubCategory> getTypeOfChannelGec = this.EDMX.Context.SubCategories.Where(c => c.category_id == 7 && c.is_parentId == 86).ToList();
                return getTypeOfChannelGec;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Get type of channel news.
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetTypeOfChannelNews()
        {
            try
            {
                List<SubCategory> getTypeOfChannelNews = this.EDMX.Context.SubCategories.Where(c => c.category_id == 7 && c.is_parentId == 87).ToList();
                return getTypeOfChannelNews;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Get Country Data from subcategories
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetCountryData()
        {
            try
            {
                List<SubCategory> getCountryData = this.EDMX.Context.SubCategories.Where(c => c.category_id == 9).ToList();
                return getCountryData;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Get telecast category
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetTelecastCategory()
        {
            try
            {
                List<SubCategory> getTelecastCategory = this.EDMX.Context.SubCategories.Where(c => c.category_id == 2).ToList();
                return getTelecastCategory;
            }
            catch (Exception ex)
            { }
            return null;
        }

        /// <summary>
        /// Get telecast schedule
        /// </summary>
        /// <returns></returns>
        public List<SubCategory> GetTelecastSchedule()
        {
            try
            {
                List<SubCategory> getTelecastSchedule = this.EDMX.Context.SubCategories.Where(c => c.category_id == 3).ToList();
                return getTelecastSchedule;
            }
            catch (Exception ex)
            { }
            return null;
        }
        #endregion


    }
}
