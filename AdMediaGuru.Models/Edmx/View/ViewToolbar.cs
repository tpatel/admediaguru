﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.ViewToolbar")]
    public partial class ViewToolbar
    {
        public int id { get; set; }

        [StringLength(1000)]
        public string name { get; set; }

        public int? viewId { get; set; }

        public int? typeId { get; set; }

        public int? componentId { get; set; }

        public int? order { get; set; }

        [StringLength(1000)]
        public string eventName { get; set; }

        public bool isDisplayInMenu { get; set; }

        public string iconUrl { get; set; }

        public string iconUrlHover { get; set; }

        [StringLength(1000)]
        public string style { get; set; }

        [StringLength(1000)]
        public string mapKey { get; set; }

        [StringLength(1000)]
        public string tooltip { get; set; }

        public string url { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public virtual Component Component { get; set; }

        public virtual Type Type { get; set; }

        public virtual View View { get; set; }
    }
}
