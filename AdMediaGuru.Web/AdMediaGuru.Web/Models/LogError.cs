﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AdMediaGuru.Web.Models
{
    public class LogError
    {   
        public string moduleName { get; set; }//controllername
        public string typeName { get; set; }//Not required
        public string methodName { get; set; }//Method name
        public string source { get; set; }//Not required
        public string message { get; set; }//ex message
        public string stackTrace { get; set; }//Not required
        public DateTime? createdDate { get; set; }//Current date.
    }
}