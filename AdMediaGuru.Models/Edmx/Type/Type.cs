﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Type.Type")]
    public partial class Type
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Type()
        {
            CampaignSettings = new HashSet<CampaignSetting>();
            Components = new HashSet<Component>();
            Forms = new HashSet<Form>();
            FormLayouts = new HashSet<FormLayout>();
            Dictionaries = new HashSet<Dictionary>();
            Languages = new HashSet<Language>();
            LanguageDictionaries = new HashSet<LanguageDictionary>();
            Errors = new HashSet<Error>();
            Properties = new HashSet<Property>();
            PropertyComponents = new HashSet<PropertyComponent>();
            Type1 = new HashSet<Type>();            
            ViewCards = new HashSet<ViewCard>();
            ViewComponents = new HashSet<ViewComponent>();
            ViewDisplayTypes = new HashSet<ViewDisplayType>();
            ViewLayouts = new HashSet<ViewLayout>();
            ViewLevels = new HashSet<ViewLevel>();
            ViewLevels1 = new HashSet<ViewLevel>();
            ViewProperties = new HashSet<ViewProperty>();
            ViewToolbars = new HashSet<ViewToolbar>();
            ViewTypeComponents = new HashSet<ViewTypeComponent>();
            ViewTypes = new HashSet<ViewType>();
        }

        public int id { get; set; }

        [StringLength(1000)]
        public string name { get; set; }

        public int? parentId { get; set; }

        [StringLength(1000)]
        public string tableName { get; set; }

        public bool? isPermitImport { get; set; }

        [StringLength(100)]
        public string setUrl { get; set; }

        public DateTime? createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public int? createdBy { get; set; }

        public int? updatedBy { get; set; }

        [StringLength(500)]
        public string systemIp { get; set; }

        public int? cardGroup { get; set; }

        public int? isLevel { get; set; }

        public int? isConfigurableInView { get; set; }

        [StringLength(1000)]
        public string getUrl { get; set; }

        [StringLength(1000)]
        public string popOverStyle { get; set; }

        public bool? isItem { get; set; }

        [StringLength(2000)]
        public string stripColor { get; set; }

        public bool? isNullAllowed { get; set; }

        public string allowedNullName { get; set; }

        public long? order { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignSetting> CampaignSettings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Component> Components { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Form> Forms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<FormLayout> FormLayouts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Dictionary> Dictionaries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Language> Languages { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LanguageDictionary> LanguageDictionaries { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Error> Errors { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Property> Properties { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PropertyComponent> PropertyComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Type> Type1 { get; set; }

        public virtual Type Type2 { get; set; }        

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewCard> ViewCards { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewComponent> ViewComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewDisplayType> ViewDisplayTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLayout> ViewLayouts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLevel> ViewLevels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLevel> ViewLevels1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewProperty> ViewProperties { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewToolbar> ViewToolbars { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewTypeComponent> ViewTypeComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewType> ViewTypes { get; set; }
    }
}
