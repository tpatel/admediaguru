﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace AdMediaGuru.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            //var cors = new EnableCorsAttribute()
            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                 name: "DefaultApi",
                 routeTemplate: "api/v1/{controller}/{id}",
                 defaults: new { id = RouteParameter.Optional }
             );

            
            config.Formatters.Clear();
            var jsonFormmater = new JsonMediaTypeFormatter();
            //jsonFormmater.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
            //jsonFormmater.SerializerSettings.DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore;
            config.Formatters.Add(jsonFormmater);

        }
    }
}
