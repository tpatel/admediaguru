﻿using AdMediaGuru.BusinessServices;
using AdMediaGuru.BusinessServices.Factory;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace AdMediaGuru.API.Authenticate
{
    public class UnAuthorizeAdMediaGuru : ApiController
    {
        #region Variable
        protected UnAuthorizedBService unAuthorizedBService;
        #endregion
        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public UnAuthorizeAdMediaGuru()
        {
            unAuthorizedBService = new UnAuthorizedBService();
        }
        #endregion

        #region Other

        //Json media type formatter.
        protected JsonMediaTypeFormatter jsonMediaTypeFormatter = new JsonMediaTypeFormatter()
        {
            SerializerSettings = {
                NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore,
                DefaultValueHandling = Newtonsoft.Json.DefaultValueHandling.Ignore,
                DateFormatString ="d MM, yyyy"
            }
        };

        /// <summary>
        /// Pass response
        /// </summary>
        /// <param name="resultType"></param>
        /// <param name="resultData"></param>
        /// <param name="messageTitle"></param>
        /// <param name="messageDescription"></param>
        /// <returns></returns>
        public HttpResponseMessage PassResponse(HttpStatusCode resultType, object resultData, string messageTitle, string messageDescription)
        {
            var response = new ResponseMessage
            {
                resultType = resultType,
                resultData = resultData,
                messageTitle = messageTitle,
                messageDescription = messageDescription
            };
            return new HttpResponseMessage(resultType)
            {
                Content = new ObjectContent<ResponseMessage>(response, jsonMediaTypeFormatter)
            };
        }
        #endregion
    }
}