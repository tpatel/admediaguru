﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdMediaGuru.Models
{
    [Table("Category.SubCategory")]
    public partial class SubCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SubCategory()
        {
            CampaignSettings = new HashSet<CampaignSetting>();
            CampaignSettings1 = new HashSet<CampaignSetting>();
            SubCategory1 = new HashSet<SubCategory>();
            ChannelProgrammes = new HashSet<ChannelProgramme>();
            ChannelProgrammes1 = new HashSet<ChannelProgramme>();
            ChannelInfoes = new HashSet<ChannelInfo>();
            ChannelInfoes1 = new HashSet<ChannelInfo>();
        }


        [Key]
        public int sub_category_id { get; set; }

        public int? category_id { get; set; }

        [StringLength(1000)]
        public string category_text { get; set; }

        [StringLength(1000)]
        public string category_description { get; set; }

        public DateTime? created_date { get; set; }

        public bool? is_deleted { get; set; }

        public int? is_parentId { get; set; }

        [StringLength(1000)]
        public string control_type { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignSetting> CampaignSettings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignSetting> CampaignSettings1 { get; set; }

        public virtual Category Category { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SubCategory> SubCategory1 { get; set; }

        public virtual SubCategory SubCategory2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChannelProgramme> ChannelProgrammes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChannelProgramme> ChannelProgrammes1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChannelInfo> ChannelInfoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChannelInfo> ChannelInfoes1 { get; set; }
    }
}
