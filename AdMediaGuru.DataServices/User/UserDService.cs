﻿using AdMediaGuru.DataServices.GenericRepository;
using AdMediaGuru.Models;
using AdMediaGuru.Models.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public class UserDService : IUserDService
    {
        #region Context
        protected GenericRepository<User> EDMXInstance { get; set; }
        public readonly Expression<Func<User, UserDTO>> selectUserExpression;
        #endregion

        #region Constructor
        public UserDService()
        {
            this.EDMXInstance = new GenericRepository<User>();
        }
        #endregion



        #region Methods

        /// <summary>
        /// Create user
        /// </summary>
        /// <param name="userData"></param>
        /// <returns></returns>
        public User CreateUser(User userData)
        {
            User objUser = new User()
            {
                user_state = userData.user_state,
                user_name = userData.user_name,
                user_company_name = userData.user_company_name,
                user_emailId = userData.user_emailId,
                user_phone = userData.user_phone,
                user_password = userData.user_password, //StandardUtility.PasswordEncoding(userData.user_password),
                is_public = true,
                is_deleted = false,
                created_date = DateTime.UtcNow
            };
            this.EDMXInstance.DbSet.Add(objUser);
            this.EDMXInstance.Context.SaveChanges();            
            return ConvertToUserModel(objUser);
        }

        /// <summary>
        /// Check if email id exists or not.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public bool CheckIfUserRegistar(string emailId)
        {
            return (this.EDMXInstance.DbSet.Count(user => user.user_emailId == emailId && user.is_deleted == false && user.is_public == true)>0 ? true:false);
        }

        /// <summary>
        /// Convert table to model.
        /// </summary>
        /// <param name="objUserModel"></param>
        /// <returns></returns>
        private User ConvertToUserModel(User objUserModel)
        {
            return new User()
            {
                user_id = objUserModel.user_id,
                user_state = objUserModel.user_state,
                user_name = objUserModel.user_name,
                user_company_name = objUserModel.user_company_name,
                user_emailId = objUserModel.user_emailId,
                user_phone = objUserModel.user_phone
            };
        }

        /// <summary>
        /// Login authenticate user.
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public User Authenticate(string emailId, string password)
        {
            //string encodedPassword = StandardUtility.PasswordEncoding(password);
            return (this.EDMXInstance.Context.Users.Where(user => user.user_emailId == emailId && 
                                                                user.user_password == password && 
                                                                user.is_deleted == false).
                                                                Select(userInfo => new UserDTO {user_id = userInfo.user_id,user_emailId = userInfo.user_emailId}).SingleOrDefault());
             
        }

        /// <summary>
        /// Function for furget password.
        /// </summary>
        /// <param name="emailId"></param>
        /// <returns></returns>
        public bool ForgetPassword(string emailId)
        {
            string randomPassword = RandomGenerator.RandomPassword();
            return (this.EDMXInstance.Context.Database.ExecuteSqlCommand($" UPDATE [User].[User] SET user_password = '{randomPassword}', updated_date = getdate() WHERE user_emailId = '{emailId}'") > 0 ? true : false);
            //return (this.EDMXInstance.Context.Database.ExecuteSqlCommand($" UPDATE [User].[User] SET user_password = '{StandardUtility.PasswordEncoding(randomPassword)}', updated_date = getdate() WHERE user_emailId = '{emailId}'")>0 ? true:false);
        }
        #endregion
    }
}
