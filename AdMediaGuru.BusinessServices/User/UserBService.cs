﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public class UserBService : BaseBService,IUserBService
    {   
        public User CreateUser(User userData)
        {
            bool createUser = _dServiceFactory.UserDService.CheckIfUserRegistar(userData.user_emailId);
            if (createUser == false)
                return _dServiceFactory.UserDService.CreateUser(userData);
            else
                return null;
        }
        public bool CheckIfUserRegistar(string emailId)
        {
            return _dServiceFactory.UserDService.CheckIfUserRegistar(emailId);
        }

        public User Authenticate(string emailId, string password)
        {
            return _dServiceFactory.UserDService.Authenticate(emailId,password);
        }

        public bool ForgetPassword(string emailId)
        {
            return _dServiceFactory.UserDService.ForgetPassword(emailId);
        }
    }
}
