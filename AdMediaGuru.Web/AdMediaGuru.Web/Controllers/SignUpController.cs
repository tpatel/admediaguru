﻿using AdMediaGuru.Models;
using AdMediaGuru.Web.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdMediaGuru.Web.Controllers
{
    public class SignUpController : AuthorizeWebAdMediaGuru
    {
        // GET: SignUp
        /// <summary>
        /// Default method.
        /// </summary>
        /// <returns></returns>
        public ActionResult SignUp()
        {
            try
            {
                List<SubCategory> getState = AuthorizeBService.SubCategoryBService.GetStateData();
                ViewBag.stateInfo = getState;
                return View();
            }
            catch (Exception ex)
            { }
            return View();
        }

        /// <summary>
        /// Create user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SignUp(User user)
        {
            try
            {
                User createUser = AuthorizeBService.UserBService.CreateUser(user);
                if (createUser != null)
                    return Json("Success", JsonRequestBehavior.AllowGet);
                else
                    return Json("UnSuccess", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            { return Json("Exception", JsonRequestBehavior.AllowGet); }
        }
    }
}