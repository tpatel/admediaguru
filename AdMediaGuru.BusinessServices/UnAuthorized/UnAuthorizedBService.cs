﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public class UnAuthorizedBService:BaseBService, IUnAuthorizedBService
    {
        public CampaignSetting CreateCampaignInfo(CampaignSetting campaignData)
        {
            return _dServiceFactory.CampaignSettingDService.CreateCampaignInfo(campaignData);
        }
    }
}
