﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.View")]
    public partial class View
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public View()
        {            
            View1 = new HashSet<View>();
            ViewCards = new HashSet<ViewCard>();
            ViewComponents = new HashSet<ViewComponent>();
            ViewLayouts = new HashSet<ViewLayout>();
            ViewLevels = new HashSet<ViewLevel>();
            ViewProperties = new HashSet<ViewProperty>();
            ViewToolbars = new HashSet<ViewToolbar>();
        }

        public int id { get; set; }

        public int? viewTypeId { get; set; }

        public int? defaultViewDisplayTypeId { get; set; }

        public int? parentId { get; set; }

        public int? dictionaryId { get; set; }

        public int? noRecordMessageDictionaryId { get; set; }

        [StringLength(4000)]
        public string name { get; set; }

        [StringLength(4000)]
        public string description { get; set; }

        [StringLength(4000)]
        public string viewMode { get; set; }

        public int? levelOneTypeId { get; set; }

        public int? levelTwoTypeId { get; set; }

        public string menuIconUrl { get; set; }

        [StringLength(1000)]
        public string key { get; set; }

        public int? order { get; set; }

        public int? groupId { get; set; }

        public string url { get; set; }

        public bool? isLanguage { get; set; }

        public string iconUrl { get; set; }

        public bool? isAllowAddInToolbar { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public string noRecordURL { get; set; }

        public string galleryUrl { get; set; }

        public string helpVideoUrl { get; set; }

        public virtual Dictionary Dictionary { get; set; }

        public virtual Dictionary Dictionary1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<View> View1 { get; set; }

        public virtual View View2 { get; set; }

        public virtual ViewDisplayType ViewDisplayType { get; set; }

        public virtual ViewType ViewType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewCard> ViewCards { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewComponent> ViewComponents { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLayout> ViewLayouts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewLevel> ViewLevels { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewProperty> ViewProperties { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ViewToolbar> ViewToolbars { get; set; }
    }
}
