﻿using AdMediaGuru.Models;

using AdMediaGuru.Web.Authorize;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdMediaGuru.Web.Controllers
{
    public class CampaignSettingController : AuthorizeWebAdMediaGuru
    {
        // GET: CampaignSetting
        public ActionResult CampaignSetting()
        {
            try
            {

                //ViewBag.channelId = Convert.ToInt32(Session["SESSION_CHANNEL_ID"]);
                List<SubCategory> getProductCategory = AuthorizeBService.SubCategoryBService.GetProductCategory();
                ViewBag.ProductCategory = getProductCategory;
                List<SubCategory> getCampaignObjective = AuthorizeBService.SubCategoryBService.GetCampaignObjective();
                ViewBag.CampaignObjective = getCampaignObjective;
                List<SubCategory> getKindOfAudience = AuthorizeBService.SubCategoryBService.GetKindOfAudience();
                ViewBag.AudienceType = getKindOfAudience;
                List<SubCategory> getRegion = AuthorizeBService.SubCategoryBService.GetRegion();
                ViewBag.RegionType = getRegion;
                List<SubCategory> getTypeOfChannel = AuthorizeBService.SubCategoryBService.GetTypeOfChannel();
                ViewBag.TypeOfChannel = getTypeOfChannel;
                List<SubCategory> getTypeOfChannelGec = AuthorizeBService.SubCategoryBService.GetTypeOfChannelGec();
                ViewBag.TypeOfChannelGec = getTypeOfChannelGec;
                List<SubCategory> getTypeOfChannelNews = AuthorizeBService.SubCategoryBService.GetTypeOfChannelNews();
                ViewBag.TypeOfChannelNews = getTypeOfChannelNews;
                return View();


            }
            catch (Exception ex)
            {

            }
            return View();
        }
        ///<summary>
        ///Save Campaign details
        /// </summary>
        /// <param></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CampaignSetting(CampaignSetting campaignData, FormCollection frmcollection)

        {
            try
            {
                var checkCampaignData = AuthorizeBService.CampaignSettingBService.CheckIfCampaignNameExists(campaignData.campaignName);
                if (checkCampaignData == true)
                    return Json("CampaignNameExists", JsonRequestBehavior.AllowGet);
                else
                {
                    //var addCampaignData = AuthorizeBService.CampaignSettingBService.CreateCampaignInfo(campaignData);

                    //if (addCampaignData != null)
                    //{

                    //Audience Table Data
                    Models.AudienceDataModel jsonData = new Models.AudienceDataModel();

                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["AllAdults"])))
                    {
                        jsonData.AllAdults = new Models.AudienceBoundData()
                        {
                            lowerRange = Convert.ToInt32(frmcollection["AllAdults"].ToString().Split(',').ToArray().First()),
                            upperRange = Convert.ToInt32(frmcollection["AllAdults"].ToString().Split(',').ToArray().Last())
                        };
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["Male"])))
                    {
                        jsonData.Male = new Models.AudienceBoundData()
                        {
                            lowerRange = Convert.ToInt32(frmcollection["Male"].ToString().Split(',').ToArray().First()),
                            upperRange = Convert.ToInt32(frmcollection["Male"].ToString().Split(',').ToArray().Last())
                        };
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["Female"])))
                    {
                        jsonData.Female = new Models.AudienceBoundData()
                        {
                            lowerRange = Convert.ToInt32(frmcollection["Female"].ToString().Split(',').ToArray().First()),
                            upperRange = Convert.ToInt32(frmcollection["Female"].ToString().Split(',').ToArray().Last())
                        };
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["YoungAdults"])))
                    {
                        jsonData.YoungAdults = new Models.AudienceBoundData()
                        {
                            lowerRange = Convert.ToInt32(frmcollection["YoungAdults"].ToString().Split(',').ToArray().First()),
                            upperRange = Convert.ToInt32(frmcollection["YoungAdults"].ToString().Split(',').ToArray().Last())
                        };
                    }

                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["Child"])))
                    {
                        jsonData.Child = new Models.AudienceBoundData()
                        {
                            lowerRange = Convert.ToInt32(frmcollection["Child"].ToString().Split(',').ToArray().First()),
                            upperRange = Convert.ToInt32(frmcollection["Child"].ToString().Split(',').ToArray().Last())
                        };
                    }

                    string jsonResult = JsonConvert.SerializeObject(jsonData);


                    //Type Of ChannelData
                    Models.TypeOfChannelDataModel channeldaytimejson = new Models.TypeOfChannelDataModel();

                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["typeOfChannelId"])))
                    {
                        channeldaytimejson.GEC = new Models.TypeOfChannelDataCategory()
                        {
                            time = frmcollection["chkGEC"].ToString().Split(',').ToArray(),
                            days = Convert.ToString(frmcollection["radioGEC"]),
                        };
                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["typeOfChannelId"])))
                    {
                        channeldaytimejson.NEWS = new Models.TypeOfChannelDataCategory()
                        {
                            time = frmcollection["chkNEWS"].ToString().Split(',').ToArray(),
                            days = Convert.ToString(frmcollection["radioNEWS"]),
                        };

                    }

                    string dayTimeJson = JsonConvert.SerializeObject(channeldaytimejson);

                    //Region Table Data
                    Models.RegionDataModel regionJsonData = new Models.RegionDataModel();
                    if (!string.IsNullOrEmpty(Convert.ToString(frmcollection["select-region"])))
                    {
                        regionJsonData.selectRegion = frmcollection["select-region"].ToString().Split(',').ToArray();
                    }
                    string regionJson = JsonConvert.SerializeObject(regionJsonData);

                    var ranges = new Audience()
                    {
                        audienceTypeJson = jsonResult
                    };

                    var channelType = new TypeOfChannelDayTime()
                    {
                        typeOfChannelDayTimeJson = dayTimeJson
                    };

                    var regionType = new Region()
                    {
                        regionSelectedJson = regionJson
                    };
                    campaignData.Audiences.Add(ranges);
                    campaignData.TypeOfChannelDayTimes.Add(channelType);
                    campaignData.Regions.Add(regionType);
                    var addCampaignData = AuthorizeBService.CampaignSettingBService.CreateCampaignInfo(campaignData);
                    //Audience audienceData = JsonConvert.DeserializeObject<Audience>(jsonResult);

                    return Json("Success", JsonRequestBehavior.AllowGet);
                    //}
                    //else
                    //    return Json("Failure", JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

    }

}