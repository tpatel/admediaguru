﻿using AdMediaGuru.DataServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public abstract class BaseBService
    {
        public BaseBService() { }

        public DServiceFactory _dServiceFactory
        {
            get { return DServiceFactory.GetDServiceFactory(); }
        }
    }
}
