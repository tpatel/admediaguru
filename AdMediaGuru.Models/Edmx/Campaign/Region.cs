﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Campaign.Region")]
    public partial class Region
    {
        [Key]
        public int campaignRegionId { get; set; }

        public int? campaignId { get; set; }

        public string regionSelectedJson { get; set; }

        public DateTime? campaignCreatedDate { get; set; }

        public DateTime? campaignUpdatedDate { get; set; }

        public int? campaignCreatedBy { get; set; }

        public bool? campaignIsDeleted { get; set; }

        public virtual CampaignSetting CampaignSetting { get; set; }
    }
}
