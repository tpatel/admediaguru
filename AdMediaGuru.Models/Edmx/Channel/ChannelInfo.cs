﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Channel.ChannelInfo")]
    public partial class ChannelInfo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ChannelInfo()
        {
            CampaignSettings = new HashSet<CampaignSetting>();
            ChannelProgrammes = new HashSet<ChannelProgramme>();
        }

        [Key]
        public int channel_id { get; set; }

        public int? user_id { get; set; }

        [StringLength(4000)]
        public string channelname { get; set; }

        public string channellogo { get; set; }

        [StringLength(4000)]
        public string contactperson { get; set; }

        public string channeladdress { get; set; }

        public int? channelcountryId { get; set; }

        public int? channelstateId { get; set; }

        public string channelemail { get; set; }

        [StringLength(20)]
        public string contactnumber { get; set; }

        public DateTime createddate { get; set; }

        public DateTime? updateddate { get; set; }

        public int? createdby { get; set; }

        public bool is_deleted { get; set; }

        [StringLength(30)]
        public string channelpassword { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignSetting> CampaignSettings { get; set; }

        public virtual SubCategory SubCategory { get; set; }

        public virtual SubCategory SubCategory1 { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChannelProgramme> ChannelProgrammes { get; set; }
    }
}
