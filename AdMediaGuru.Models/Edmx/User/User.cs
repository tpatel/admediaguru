﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("User.User")]
    public partial class User
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public User()
        {
            CampaignSettings = new HashSet<CampaignSetting>();
            ChannelInfoes = new HashSet<ChannelInfo>();
            ChannelInfoes1 = new HashSet<ChannelInfo>();
        }

        [Key]
        public int user_id { get; set; }

        [StringLength(500)]
        public string user_state { get; set; }

        [StringLength(4000)]
        public string user_name { get; set; }

        [StringLength(2000)]
        public string user_company_name { get; set; }

        public string user_emailId { get; set; }

        [StringLength(20)]
        public string user_phone { get; set; }

        [StringLength(100)]
        public string user_password { get; set; }

        public bool is_public { get; set; }

        public bool is_deleted { get; set; }

        public DateTime? created_date { get; set; }

        public DateTime? updated_date { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignSetting> CampaignSettings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChannelInfo> ChannelInfoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChannelInfo> ChannelInfoes1 { get; set; }
    }
}
