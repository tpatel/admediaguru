﻿using AdMediaGuru.BusinessServices.Factory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AdMediaGuru.Web.Authorize
{
    public class AuthorizeWebAdMediaGuru : Controller
    {
        #region Variable
        private BServiceFactory _bServiceFactory = null;
        #endregion

        #region Setter
        public BServiceFactory AuthorizeBService
        {
            get
            {
                if (_bServiceFactory == null)
                    _bServiceFactory = BServiceFactory.GetBServiceFactory();
                return _bServiceFactory;
            }
        }
        #endregion
    }
}