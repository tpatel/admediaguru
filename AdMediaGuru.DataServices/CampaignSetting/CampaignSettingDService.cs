﻿using AdMediaGuru.DataServices.GenericRepository;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public class CampaignSettingDService : ICampaignSettingDService
    {
        #region Context
        protected GenericRepository<CampaignSetting> EDMXInstanceCampaignName { get; set; }
        //public readonly Expression<Func<CampaignSetting, CampaignSettingDTO>> selectUserExpression;
        #endregion

        #region Constructor
        public CampaignSettingDService()
        {
            this.EDMXInstanceCampaignName = new GenericRepository<CampaignSetting>();
        }
        #endregion

        #region Methods

        /// <summary>
        /// CreateCampaignInfo
        /// </summary>
        /// <param name="campaignDataCreate"></param>
        /// <returns></returns>
        public CampaignSetting CreateCampaignInfo(CampaignSetting campaignDataCreate)
        {
            try
            {
                CampaignSetting campaignSettingData = new CampaignSetting()
                {
                    campaignName = campaignDataCreate.campaignName,
                    campaignBrandName = campaignDataCreate.campaignBrandName,
                    productCategoryId = campaignDataCreate.productCategoryId,
                    campaignObjectiveId = campaignDataCreate.campaignObjectiveId,
                    campaignBudget = campaignDataCreate.campaignBudget,
                    campaignDuration = campaignDataCreate.campaignDuration,
                    campaignTotalFctSeconds = campaignDataCreate.campaignTotalFctSeconds,
                    campaignTotalSpots = campaignDataCreate.campaignTotalSpots,
                    campaignStartDate = campaignDataCreate.campaignStartDate,
                    campaignEndDate = campaignDataCreate.campaignEndDate,
                    campaignCreatedDate = DateTime.UtcNow,
                    campaignIsDeleted = false,
                    Audiences = campaignDataCreate.Audiences,
                    TypeOfChannelDayTimes = campaignDataCreate.TypeOfChannelDayTimes,
                    Regions = campaignDataCreate.Regions
                };
                this.EDMXInstanceCampaignName.DbSet.Add(campaignSettingData);
                this.EDMXInstanceCampaignName.Context.SaveChanges();
                return CampaignSettingModel(campaignSettingData);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// CampaignSettingModel
        /// </summary>
        /// <param name="objCampaignSettingModel"></param>
        /// <returns></returns>
        private CampaignSetting CampaignSettingModel(CampaignSetting objCampaignSettingModel)
        {
            return new CampaignSetting()
            {
                campaignId = objCampaignSettingModel.campaignId

            };
        }
        /// <summary>
        /// Check if campaign name exists or not.
        /// </summary>
        /// <param name="campaignName"></param>
        /// <returns></returns>
        public bool CheckIfCampaignNameExists(string campaignName)
        {
            return (this.EDMXInstanceCampaignName.DbSet.Count(campaigndata => campaigndata.campaignName == campaignName) > 0 ? true : false);
        }
        #endregion
    }
}
