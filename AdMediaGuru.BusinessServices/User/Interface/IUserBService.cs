﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public interface IUserBService
    {   
        User CreateUser(User userData);
        bool CheckIfUserRegistar(string emailId);
        User Authenticate(string emailId, string password);
        bool ForgetPassword(string emailId);
    }
}
