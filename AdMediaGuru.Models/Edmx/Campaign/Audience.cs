﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("Campaign.Audience")]
    public partial class Audience
    {
        public int audienceId { get; set; }

        public int? campaignId { get; set; }

        public string audienceTypeJson { get; set; }

        public DateTime? campaignCreatedDate { get; set; }

        public DateTime? campaignUpdatedDate { get; set; }

        public int? campaignCreatedBy { get; set; }

        public bool? campaignIsDeleted { get; set; }

        public virtual CampaignSetting CampaignSetting { get; set; }
    }
}
