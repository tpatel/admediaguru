﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models
{
    [Table("View.ViewTypeComponent")]
    public partial class ViewTypeComponent
    {
        public int id { get; set; }

        public int? viewTypeId { get; set; }

        public int? componentId { get; set; }

        public string name { get; set; }

        public int? order { get; set; }

        public DateTime createdDate { get; set; }

        public DateTime? updatedDate { get; set; }

        public bool isDeleted { get; set; }

        public int? typeId { get; set; }

        public virtual Component Component { get; set; }

        public virtual Type Type { get; set; }

        public virtual ViewType ViewType { get; set; }
    }
}
