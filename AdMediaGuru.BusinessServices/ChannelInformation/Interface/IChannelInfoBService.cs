﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public interface IChannelInfoBService
    {
        ChannelInfo CreateChannelData(ChannelInfo channelData);
        bool CheckIfChannelExists(string channelName);
        bool CheckIfChannelUserRegistar(string emailId);
    }
}
