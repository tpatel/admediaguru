﻿
using AdMediaGuru.Web.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AdMediaGuru.Models;

namespace AdMediaGuru.Web.Controllers
{
    public class ChannelInformationController : AuthorizeWebAdMediaGuru
    {
        // GET: ChannelInformation
        public ActionResult ChannelInformation()
        {
            try
            {
                    System.Web.HttpContext.Current.Session["SESSION_CURRENTUSER_USER_ID"] = "";
                    System.Web.HttpContext.Current.Session["SESSION_CURRENTUSER_EMAIL_ID"] = "";
                    List<SubCategory> getState = AuthorizeBService.SubCategoryBService.GetStateData();
                    ViewBag.stateInfo = getState;
                    List<SubCategory> getCountry = AuthorizeBService.SubCategoryBService.GetCountryData();
                    ViewBag.countryInfo = getCountry;
                    return View();
            }
            catch (Exception ex)
            {
            }
            return View();
        }

        /// <summary>
        /// Save channel information 
        /// </summary>
        /// <param name="channelData"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChannelInformation(ChannelInfo channelData)
        {
            try
            {
                var checkChannelData = AuthorizeBService.ChannelInfoBService.CheckIfChannelExists(channelData.channelname);
                if (checkChannelData == true)
                    return Json("ChannelExists",JsonRequestBehavior.AllowGet);
                var checkChannelUserData = AuthorizeBService.ChannelInfoBService.CheckIfChannelUserRegistar(channelData.channelemail);
                if (checkChannelUserData == true)
                    return Json("UserExists", JsonRequestBehavior.AllowGet);
                    if (!string.IsNullOrEmpty(channelData.channelname))
                    {
                        var addChannelData = AuthorizeBService.ChannelInfoBService.CreateChannelData(channelData);
                    if (addChannelData != null)
                    {   
                        CurrentUser.SetChannelInformation(addChannelData.channel_id);
                        CurrentUser.SetSessionKeys(Convert.ToInt32(addChannelData.user_id), addChannelData.channelemail);
                        return Json("Success",JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json("Failure", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            { }
            return View();
        }
    }
}