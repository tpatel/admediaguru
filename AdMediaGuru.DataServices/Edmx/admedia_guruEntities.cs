﻿namespace AdMediaGuru.DataServices.Edmx
{
    using AdMediaGuru.Models;
    using System.Data.Entity;

    public partial class admedia_guruEntities : DbContext
    {
        public virtual DbSet<Audience> Audiences { get; set; }
        public virtual DbSet<CampaignSetting> CampaignSettings { get; set; }
        public virtual DbSet<Region> Regions { get; set; }
        public virtual DbSet<TypeOfChannelDayTime> TypeOfChannelDayTimes { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<SubCategory> SubCategories { get; set; }
        public virtual DbSet<ChannelInfo> ChannelInfoes { get; set; }
        public virtual DbSet<ChannelProgramme> ChannelProgrammes { get; set; }
        public virtual DbSet<Component> Components { get; set; }
        //public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<Form> Forms { get; set; }
        public virtual DbSet<FormLayout> FormLayouts { get; set; }
        public virtual DbSet<Dictionary> Dictionaries { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<LanguageDictionary> LanguageDictionaries { get; set; }
        public virtual DbSet<Error> Errors { get; set; }
        public virtual DbSet<Property> Properties { get; set; }
        public virtual DbSet<PropertyComponent> PropertyComponents { get; set; }
        public virtual DbSet<Type> Types { get; set; }
        public virtual DbSet<User> Users { get; set; }
        //public virtual DbSet<UserView> UserViews { get; set; }
        public virtual DbSet<View> Views { get; set; }
        public virtual DbSet<ViewCard> ViewCards { get; set; }
        public virtual DbSet<ViewComponent> ViewComponents { get; set; }
        public virtual DbSet<ViewDisplayType> ViewDisplayTypes { get; set; }
        public virtual DbSet<ViewLayout> ViewLayouts { get; set; }
        public virtual DbSet<ViewLevel> ViewLevels { get; set; }
        public virtual DbSet<ViewProperty> ViewProperties { get; set; }
        public virtual DbSet<ViewToolbar> ViewToolbars { get; set; }
        public virtual DbSet<ViewType> ViewTypes { get; set; }
        public virtual DbSet<ViewTypeComponent> ViewTypeComponents { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CampaignSetting>()
                .Property(e => e.campaignBudget)
                .HasPrecision(18, 4);

            modelBuilder.Entity<SubCategory>()
                .HasMany(e => e.CampaignSettings)
                .WithOptional(e => e.SubCategory)
                .HasForeignKey(e => e.campaignObjectiveId);

            modelBuilder.Entity<SubCategory>()
                .HasMany(e => e.CampaignSettings1)
                .WithOptional(e => e.SubCategory1)
                .HasForeignKey(e => e.productCategoryId);

            modelBuilder.Entity<SubCategory>()
                .HasMany(e => e.SubCategory1)
                .WithOptional(e => e.SubCategory2)
                .HasForeignKey(e => e.is_parentId);

            modelBuilder.Entity<SubCategory>()
                .HasMany(e => e.ChannelProgrammes)
                .WithOptional(e => e.SubCategory)
                .HasForeignKey(e => e.channelprogrammetypecategoryId);

            modelBuilder.Entity<SubCategory>()
                .HasMany(e => e.ChannelProgrammes1)
                .WithOptional(e => e.SubCategory1)
                .HasForeignKey(e => e.channeltelecastscheduleId);

            modelBuilder.Entity<SubCategory>()
                .HasMany(e => e.ChannelInfoes)
                .WithOptional(e => e.SubCategory)
                .HasForeignKey(e => e.channelcountryId);

            modelBuilder.Entity<SubCategory>()
                .HasMany(e => e.ChannelInfoes1)
                .WithOptional(e => e.SubCategory1)
                .HasForeignKey(e => e.channelstateId);

            modelBuilder.Entity<ChannelInfo>()
                .HasMany(e => e.CampaignSettings)
                .WithOptional(e => e.ChannelInfo)
                .HasForeignKey(e => e.channelId);

            modelBuilder.Entity<ChannelInfo>()
                .HasMany(e => e.ChannelProgrammes)
                .WithOptional(e => e.ChannelInfo)
                .HasForeignKey(e => e.channelId);

            modelBuilder.Entity<Dictionary>()
                .HasMany(e => e.PropertyComponents)
                .WithOptional(e => e.Dictionary)
                .HasForeignKey(e => e.dictionaryId);

            modelBuilder.Entity<Dictionary>()
                .HasMany(e => e.PropertyComponents1)
                .WithOptional(e => e.Dictionary1)
                .HasForeignKey(e => e.validationMessageDictionaryId);

            modelBuilder.Entity<Dictionary>()
                .HasMany(e => e.Views)
                .WithOptional(e => e.Dictionary)
                .HasForeignKey(e => e.dictionaryId);

            modelBuilder.Entity<Dictionary>()
                .HasMany(e => e.Views1)
                .WithOptional(e => e.Dictionary1)
                .HasForeignKey(e => e.noRecordMessageDictionaryId);

            modelBuilder.Entity<Property>()
                .HasMany(e => e.ViewLevels)
                .WithOptional(e => e.Property)
                .HasForeignKey(e => e.levelOnePriorityId);

            modelBuilder.Entity<Property>()
                .HasMany(e => e.ViewLevels1)
                .WithOptional(e => e.Property1)
                .HasForeignKey(e => e.levelTwoPriorityId);

            modelBuilder.Entity<Type>()
                .HasMany(e => e.Type1)
                .WithOptional(e => e.Type2)
                .HasForeignKey(e => e.parentId);

            //modelBuilder.Entity<Type>()
            //    .HasMany(e => e.UserViews)
            //    .WithRequired(e => e.Type)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<Type>()
                .HasMany(e => e.ViewLevels)
                .WithOptional(e => e.Type)
                .HasForeignKey(e => e.levelOneTypeId);

            modelBuilder.Entity<Type>()
                .HasMany(e => e.ViewLevels1)
                .WithOptional(e => e.Type1)
                .HasForeignKey(e => e.levelTwoTypeId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.CampaignSettings)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.userId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ChannelInfoes)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.user_id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.ChannelInfoes1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.user_id);

            //modelBuilder.Entity<View>()
            //    .HasMany(e => e.UserViews)
            //    .WithRequired(e => e.View)
            //    .WillCascadeOnDelete(false);

            modelBuilder.Entity<View>()
                .HasMany(e => e.View1)
                .WithOptional(e => e.View2)
                .HasForeignKey(e => e.parentId);

            modelBuilder.Entity<ViewDisplayType>()
                .HasMany(e => e.Views)
                .WithOptional(e => e.ViewDisplayType)
                .HasForeignKey(e => e.defaultViewDisplayTypeId);
        }
    }
}
