﻿using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.BusinessServices
{
    public class ChannelProgrammeBService : BaseBService , IChannelProgrammeBService
    {
        public ChannelProgramme CreateChannelProgrammeData(ChannelProgramme channelProgrammeData)
        {
            bool createchanneldata = _dServiceFactory.ChannelProgrammeDService.CheckIfProgrammeExists(channelProgrammeData.channelprogrammename);
            if (createchanneldata == false)
                return _dServiceFactory.ChannelProgrammeDService.CreateChannelProgrammeData(channelProgrammeData);
            else
               return null;
        }
        public bool CheckIfProgrammeExists(string programmeName)
        {
            return _dServiceFactory.ChannelProgrammeDService.CheckIfProgrammeExists(programmeName);
        }
    }
}
