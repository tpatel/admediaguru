﻿using AdMediaGuru.Models;
using AdMediaGuru.Web.Authorize;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace AdMediaGuru.Web.Controllers
{
    public class HomeController : AuthorizeWebAdMediaGuru
    {
        /// <summary>
        /// Home page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            return View();
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="emailId"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public ActionResult Login(string emailId, string password)
        {
            try
            {  
               User userDetails =  AuthorizeBService.UserBService.Authenticate(emailId, password);
                if (userDetails != null)
                {
                    CurrentUser.SetSessionKeys(userDetails.user_id, userDetails.user_emailId);
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                    return Json("Failure", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Log out click.
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            HttpContext.Session["SESSION_CURRENTUSER_USER_ID"] = "";
            HttpContext.Session["SESSION_CURRENTUSER_EMAIL_ID"] = "";
            return RedirectToAction("Home", "Home");
        }
    }
}