﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.Models 
{
    [Table("Language.Language")]
    public partial class Language
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Language()
        {
            LanguageDictionaries = new HashSet<LanguageDictionary>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public byte id { get; set; }

        [StringLength(500)]
        public string name { get; set; }

        [StringLength(50)]
        public string abbrevation { get; set; }

        [StringLength(2000)]
        public string description { get; set; }

        public DateTime? created_date { get; set; }

        public DateTime? updated_date { get; set; }

        public bool? is_deleted { get; set; }

        public int? typeId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LanguageDictionary> LanguageDictionaries { get; set; }

        public virtual Type Type { get; set; }
    }
}
