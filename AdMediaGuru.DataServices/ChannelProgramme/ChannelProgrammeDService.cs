﻿using AdMediaGuru.DataServices.GenericRepository;
using AdMediaGuru.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AdMediaGuru.DataServices
{
    public class ChannelProgrammeDService : IChannelProgrammeDService
    {
        #region Context
        protected GenericRepository<ChannelProgramme> EDMXInstanceChannelProgramme { get; set; }
        #endregion

        #region Constructor
        public ChannelProgrammeDService()
        {
            this.EDMXInstanceChannelProgramme = new GenericRepository<ChannelProgramme>();
        }
        #endregion

        #region Methods
        ///<summary>
        ///Create Channel Programme Data
        /// </summary>
        /// <returns></returns>
        public ChannelProgramme CreateChannelProgrammeData(ChannelProgramme channelProgrammeData)
        {
            try
            {
                ChannelProgramme objChannelProgrammeData = new ChannelProgramme()
                {
                    channelprogrammename = channelProgrammeData.channelprogrammename,
                    channelId = channelProgrammeData.channelId,
                    channeltelecasttime = channelProgrammeData.channeltelecasttime,
                    channelprimetimefrom = channelProgrammeData.channelprimetimefrom,
                    channelprimetimeto = channelProgrammeData.channelprimetimeto,
                    channelnonprimetimefrom = channelProgrammeData.channelnonprimetimefrom,
                    channelnonprimetimeto = channelProgrammeData.channelnonprimetimeto,
                    channelprogrammetypecategoryId = channelProgrammeData.channelprogrammetypecategoryId,
                    channeltelecastscheduleId = channelProgrammeData.channeltelecastscheduleId
                };
                this.EDMXInstanceChannelProgramme.DbSet.Add(objChannelProgrammeData);
                this.EDMXInstanceChannelProgramme.Context.SaveChanges();

                return ConvertChannelProgrammeModel(objChannelProgrammeData);
            }
            catch (Exception ex)
            { }
            return null;
        }
        /// <summary>
        /// Convert channel programme model.
        /// </summary>
        /// <param name="newChannelInfo"></param>
        /// <returns></returns>
        private ChannelProgramme ConvertChannelProgrammeModel(ChannelProgramme newChannelProgramme)
        {
            return new ChannelProgramme()
            {
                channelprogrammeid = newChannelProgramme.channelprogrammeid
            };
        }
        /// <summary>
        /// Check if programme name exists or not.
        /// </summary>
        /// <param name="programmeName"></param>
        /// <returns></returns>
        public bool CheckIfProgrammeExists(string programmeName)
        {
            return (this.EDMXInstanceChannelProgramme.DbSet.Count(channelprogrammedata => channelprogrammedata.channelprogrammename == programmeName) > 0 ? true : false);
        }
        
        #endregion
    }
}
